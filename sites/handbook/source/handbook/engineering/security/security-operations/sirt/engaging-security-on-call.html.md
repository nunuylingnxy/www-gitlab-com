---
layout: handbook-page-toc
title: Engaging the Security Engineer On-Call
description: How to Engage the Security Engineer On-Call 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Overview
The [Security Incident Response Team (SIRT)](/handbook/engineering/security/security-operations/sirt) is on-call [24/7/365](/handbook/on-call/#security-team-on-call-rotation) to assist with any security incidents. This handbook provides guidance to help identify the scope and severity of a potential security incident, followed by instructions on how to engage the Security Engineer On-Call (SEOC) if needed.

Information about SIRT responsibilities and incident ownership is available in the [SIRT On-Call Guide](/handbook/engineering/security/secops-oncall.html).

## Incident Severity
Before engaging the SEOC, please review the following to determine the level of engagement needed:

| Severity | Description | Examples | Action |
|----------|-------------|----------|--------|
| High     | Critical issues that may affect the confidentiality, integrity, or availability of GitLab services or data | 1. GitLab.com is down for all customers<br><br>2. Confidentiality or Privacy is breached<br><br>3. Unauthorized access<br><br>4. Data loss<br><br>5. Leaked credentials | See [Engage the SEOC](#engage-the-security-engineer-on-call) |
| Low      | Non-urgent issues that have minimal to no impact on GitLab services or data | 1. Third party vendor vulnerability<br><br> 2. Phishing<br><br>3. Customer inquiries<br><br>4. Troubleshooting device security<br><br> 5. Security training questions | For phishing related issues, see [Phishing](#phishing)<br><br>For other non-urgent issues, see [Low Severity Issues](#low-severity-issues) |

The following are out of scope for the SIRT and should be escalated to the respective teams:

- **Vulnerability reports and HackerOne**: please escalate to [Application Security](/handbook/engineering/security/#vulnerability-reports-and-hackerone)
- **Abuse reports and DMCA notices**: please escalate to [Trust & Safety](/handbook/engineering/security/security-operations/trustandsafety)
- **General Customer Inquiries**: please escalate to [Risk & Field Security](/handbook/engineering/security/security-assurance/risk-field-security/)
- **Self-Managed Instances**: please escalate to [Risk & Field Security](/handbook/engineering/security/security-assurance/risk-field-security/) or [Application Security](/handbook/engineering/security/security-engineering-and-research/application-security/)

## Low Severity Issues
For general Q&A, GitLab Security is available in the `#security` channel in GitLab Slack.

For low severity, non-urgent issues, [SIRT](/handbook/engineering/security/security-operations/sirt) can be reached by mentioning `@sirt-team` in Slack or by [opening an issue](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/issues/new?issue%5Bmilestone_id%5D=).

Please be advised the SLA for Slack mentions is **6 hours** on business days.

## Phishing
If you suspect you've received a phishing email and have not engaged with the sender, please see: [What to do if you suspect an email is a phishing attack](/handbook/security/#what-to-do-if-you-suspect-an-email-is-a-phishing-attack).

If you have engaged a phisher by replying to an email, clicking on a link, have sent and received text messages, or have purchased goods requested by the phisher, please [engage the SEOC](#engage-the-security-engineer-on-call).

## Engage the Security Engineer On-Call
If you have identified a high severity security incident or you need immediate assistance from the SIRT, there are two options available to engage the SEOC:

- **Slack**: use the `/security` slash command
    - Example: `/security Hi Security, I have a concern! Please see the following URL ...`
- **Email**: send an email with a brief description of the issue to `page-security@gitlab.com`

Both options will trigger the same process and page the SEOC. The SEOC will engage in the relevant issue within the appropriate [SLA](/handbook/on-call/#security-team-on-call-rotation). If the SLA is breached, the [Security Manager On-Call (SMOC)](/handbook/on-call/#security-managers) will be paged.

Paging the SEOC creates a new issue to track the incident being reported. Please provide as much detail as possible in this issue to aid the SEOC in their investigation of the incident.

The SEOC will typically respond to the page within **15 minutes** and may have questions which require synchronous communication from the incident reporter. It is important when paging the SEOC that the incident reporter be prepared and available for this synchronous communication in the initial stage of the incident response.
